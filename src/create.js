import { useState } from "react";
import{useHistory} from "react-router-dom";

const Create = () => {
    const [title,setTitle ]= useState('');
    const [body,setBody ]= useState('');
 
    const history = useHistory();
    const HandleSubmit =(e)=>{
        e.preventDefault();
        const notes ={title,body}
         fetch('http://localhost:8000/notes',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(notes)
         })
         .then(()=>{
             console.log('added');
             history.push('/');
         })
         
    }
 
    return ( 
        <div className="create">
            <h2> add a new blog </h2>
            <form onSubmit={HandleSubmit}>
                <label>title : </label>
                <input type="text" required value={title} onChange={(e)=>setTitle(e.target.value)}></input>
                <label>body : </label>
               <textarea required value={body}onChange={(e)=>setBody(e.target.value)} ></textarea>
               <button> Add Notes </button>
               <h2>title :{title}</h2>
               <h3>body:{body}</h3>
            </form>
        </div>
     );
}
 
export default Create;