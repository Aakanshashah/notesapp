import NotesList from './Noteslist';
import useFatch from './useFatch';
const Home = () => {
    const {data:notes}=useFatch('http://localhost:8000/notes');
   
    
    return (
        <div className="home">
            <h1>Home Page</h1>
            {notes && <NotesList notes={notes} title="all notes "></NotesList>}

        </div>
    );
}

export default Home;