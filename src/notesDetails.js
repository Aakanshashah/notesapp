import { useParams } from "react-router";
import useFetch from "./useFatch";
import{useHistory} from "react-router-dom";

const NotesDetails = () => {
    const { id } = useParams();
    const { data: notes } = useFetch('http://localhost:8000/notes/' + id);
    const history = useHistory();
    const handleDelete = () => {
        // e.preventDefault();
        fetch('http://localhost:8000/notes/' + notes.id, {
            method: 'DELETE',
        })
            .then(function (res) {
                return res.json();
            })
            .then(function (data) {
                console.log(data)
                history.push('/');
            })
    }
    return (
        <div className="notesDetails">
            {/* <h2>notes details - {id}</h2> */}
            {notes && <article>
                <h2>{notes.title}</h2>
                <h3>{notes.body} <br></br></h3>
                <button onClick={handleDelete}>delete</button>
            </article>}

        </div>
    );
}

export default NotesDetails;