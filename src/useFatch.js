import { useState, useEffect } from 'react';

const useFetch = (link) => {
    const [data, setBlog] = useState(null);
    


    useEffect (() => {
     
        fetch(link)
        .then((res)=>{
            return res.json();
        })
        .then((data)=>{
         
            setBlog(data);
            
        })
    
        
     },[link]);


    return {data};
}
 
export default useFetch;