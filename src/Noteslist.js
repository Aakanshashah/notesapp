import { Link } from "react-router-dom";

const NotesList = (props) => {
    const notes = props.notes;
    const title = props.title;
    
  
    return (
    <div className="notes-list">
    <h3>{title}</h3>
        {notes.map((item) => (
            <div className="notes-preview" key={item.id} >
            <Link to ={`/notes/${item.id}`}>  <h2>{item.title}</h2></Link>
              
            </div>
        ))}

    </div>

    );
}

export default NotesList;