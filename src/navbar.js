const Navbar = () => {

    return (
        <div className="Navbar">
            <h2>Notes App </h2>
            <div className="links">
                <a href ="/">Home</a>
                <a href ="/Create">New Notes</a>
            </div>
        </div>
    );
}
 
export default Navbar;