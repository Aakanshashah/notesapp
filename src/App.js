import './App.css';
import Navbar from './navbar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './home';
import Create from './create';
import NotesDetails from './notesDetails';

function App() {
  return (
    <Router>

      <div className="App">
        <Navbar />
        <div className="content">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/Create">
              <Create/>
            </Route>
            <Route path="/notes/:id">
              <NotesDetails/>
            </Route>

          </Switch>

        </div>
      </div>
    </Router>

  );
}

export default App;
